$(document).ready(init)

const INITIAL_TN = 100,
    RANGE_TN = 50,
    INITIAL_MATERIAL_TN = 55,
    MAX_MATERIALS = 3

let materials = ['дерево', 'камень', 'медь', 'серебро', 'золото', 'сталь', 'кость'],
    monsterAdjectives = ['гнусный', 'пидор', 'говноед', 'железножопый', 'глиномес', 'сучара'],
    monsterNouns = ['йети', 'дракон', 'медведос', 'волчара', 'бандос']

let selectedMaterials = [],
    selectedAdjective,
    selectedNoun

let targetNumbers = { nouns: {}, adjectives: {}, materials: {} }

let currentResult,
    currentTN

let knowledge = {}

function init() {
    initButtons()
    initMaterials()
    initTargets()

    generateOrder()
}

function initButtons() {

    $('#generate_button').click(() => {
        generateOrder()
    })

    $('#create_button').click(() => {
        create()
    })

    $('#reset_button').click(() => {
        selectedMaterials.length = 0
        updateSelectedMaterials()
    })

    $('#simulate_button').click(() => {
        simulate()
    })

    $('#log_button').click(() => {
        console.table(targetNumbers.nouns)
        console.table(targetNumbers.adjectives)
        Object.keys(targetNumbers.materials).forEach(material => { console.log(material); console.table(targetNumbers.materials[material]) } )
    })
}

function initTargets() {
    monsterNouns.forEach(noun => {
        targetNumbers.nouns[noun] = getRandomTN(INITIAL_TN, RANGE_TN)
    })

    monsterAdjectives.forEach(adjective => {
        targetNumbers.adjectives[adjective] = getRandomTN(INITIAL_TN, RANGE_TN)
    })

    materials.forEach(material => {
        targetNumbers.materials[material] = {}

        monsterNouns.forEach(noun => {
            targetNumbers.materials[material][noun] = getRandomTN(INITIAL_MATERIAL_TN, RANGE_TN)
        })

        monsterAdjectives.forEach(adjective => {
            targetNumbers.materials[material][adjective] = getRandomTN(INITIAL_MATERIAL_TN, RANGE_TN)
        })

    })
}

function getSelectedMonster() {
    return { noun: selectedNoun, adj: selectedAdjective }
}

function create() {
    if (selectedMaterials.length < MAX_MATERIALS) {
        return
    }

    let materialResults = selectedMaterials.reduce((acc, material) => {
        acc[material] = getMaterialTN(material, getSelectedMonster())
        updateKnowledge(material, selectedNoun, targetNumbers.materials[material][selectedNoun])
        updateKnowledge(material, selectedAdjective, targetNumbers.materials[material][selectedAdjective])
        return acc
    }, {})

    currentResult = selectedMaterials.reduce((sum, material) => {
        sum += materialResults[material]
        return sum
    }, 0)

    let breakdown = selectedMaterials.reduce((acc, material) => {
        acc.push(`${materialResults[material]} from ${material}`)
        return acc
    }, [])

    ultra_result.innerHTML = `Result: ${currentResult}; (${breakdown.join(', ')})`
}

function updateKnowledge(material, target, value) {
    if (knowledge[material] && knowledge[material][target]) {
        return;
    }

    if (!knowledge[material]) {
        knowledge[material] = {}
    }

    knowledge[material][target] = Math.floor(value / 25)

    generateKnowledgeTable()
}

function generateKnowledgeTable() {
    let pool = $('#knowledge_pool'),
        row = null

    pool[0].innerHTML = ''

    Object.keys(knowledge)
        .filter(material => knowledge[material][selectedNoun] !== undefined || knowledge[material][selectedAdjective] !== undefined)
        .forEach((material, materialIndex) => {
            (materialIndex % 3 === 0) && (row = addRow(pool))
            addMaterialKnowledge(row, material, knowledge[material][selectedNoun], knowledge[material][selectedAdjective])
        })
}

function getRandomTN(init, range) {
    return init + Math.floor(Math.random() * range * 2) - range
}

function generateOrder() {
    selectedAdjective = getRandom(monsterAdjectives)
    selectedNoun = getRandom(monsterNouns)

    updateOrder()
}

function getRandom(array) {
    return array[Math.floor(Math.random() * array.length)]
}

function updateOrder() {
    target_adjective.innerHTML = selectedAdjective
    target_noun.innerHTML = selectedNoun

    currentTN = getMonsterTN(getSelectedMonster())
    target_number.innerHTML = `${currentTN} (${targetNumbers.nouns[selectedNoun]} from '${selectedNoun}', ${targetNumbers.adjectives[selectedAdjective]} from '${selectedAdjective}')`

    generateKnowledgeTable()
}

function initMaterials() {
    let pool = $('#material_pool'),
        row = null

    materials.forEach((material, materialIndex) => {
        (materialIndex % 3 === 0) && (row = addRow(pool))
        addMaterial(row, material)
    })
}

function selectMaterial(material) {
    if (selectedMaterials.indexOf(material) === -1 && selectedMaterials.length < MAX_MATERIALS) {
        selectedMaterials.push(material)
        selectedMaterials = selectedMaterials.sort()
        updateSelectedMaterials()
    }
}

function updateSelectedMaterials() {
    selected_materials.innerHTML = selectedMaterials.join(' ')
}

function addRow(pool) {
    let row = $('<div></div>').addClass('material__pool-row')
    pool.append(row)

    return row
}

function addMaterial(row, name) {
    let material = $('<div></div>').addClass('material__item')
        .append($(`<button>${name}</button>`)
            .addClass('btn')
            .click(() => { selectMaterial(name) }))

    row.append(material);
}

function addMaterialKnowledge(row, name, noun, adj) {
    let material = $('<div></div>').addClass('material__item')
        .append($(`<span>${name}</span>`))
        .append($(`<span>■</span>`).addClass(`material__knowledge ${getKnowledgeClass(adj)}`))
        .append($(`<span>■</span>`).addClass(`material__knowledge ${getKnowledgeClass(noun)}`))

    row.append(material);
}

function getKnowledgeClass(value) {
    switch (value) {
        case 0:
            return 'bad'

        case 1:
            return 'medium'

        case 2:
            return 'good'

        case 3:
        case 4:
            return 'excellent'

        default:
            return 'unknown'
    }
}

function simulate() {
    let iterations = 100000,
        successes = 0

    for (let i = 0; i < iterations; ++i) {
        let monster = getRandomMonster(),
            weapon = getRandomWeapon(),
            monsterTN = getMonsterTN(monster),
            weaponTN = getWeaponTN(weapon, monster)

        weaponTN >= monsterTN && ++successes
    }

    let simulationResult = successes / iterations

    statistics_pool.innerHTML = `Successes: ${simulationResult}`

    function getRandomMonster() {
        return { noun: getRandom(monsterNouns), adj: getRandom(monsterAdjectives) }
    }

    function getRandomWeapon() {
        let materialsCopy = materials.slice(),
            result = []

        for (let i = 0; i < MAX_MATERIALS; ++i) {
            let material = getRandom(materialsCopy)
            materialsCopy.splice(materialsCopy.indexOf(material), 1)
            result.push(material)
        }

        return result
    }
}

function getMonsterTN(monster) {
    return targetNumbers.nouns[monster.noun] + targetNumbers.adjectives[monster.adj]
}

function getWeaponTN(weapon, monster) {
    return weapon.reduce((res, material) => {
        res += getMaterialTN(material, monster)
        return res
    }, 0)
}

function getMaterialTN(material, monster) {
    return (targetNumbers.materials[material][monster.noun] + targetNumbers.materials[material][monster.adj]) / 2
}